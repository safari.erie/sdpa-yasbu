-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for sdpa
CREATE DATABASE IF NOT EXISTS `sdpa` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `sdpa`;

-- Dumping structure for table sdpa.t_anggota
CREATE TABLE IF NOT EXISTS `t_anggota` (
  `id_anggota` int(5) NOT NULL AUTO_INCREMENT,
  `nbm_anggota` varchar(10) NOT NULL,
  `nama_anggota` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `jenkel` char(1) NOT NULL,
  `no_anggota` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status_user` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_anggota`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_anggota: ~19 rows (approximately)
/*!40000 ALTER TABLE `t_anggota` DISABLE KEYS */;
INSERT INTO `t_anggota` (`id_anggota`, `nbm_anggota`, `nama_anggota`, `alamat`, `jenkel`, `no_anggota`, `email`, `status_user`) VALUES
	(21, '000', 'Admin', 'Tasik', 'L', '081367561423', 'admin@gmail.com', 1);
/*!40000 ALTER TABLE `t_anggota` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_anggota_jabatan
CREATE TABLE IF NOT EXISTS `t_anggota_jabatan` (
  `id_anggota_jabatan` int(5) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(5) NOT NULL,
  `id_jabatan` int(5) NOT NULL,
  `id_tahun` int(5) NOT NULL,
  PRIMARY KEY (`id_anggota_jabatan`),
  KEY `id_anggota` (`id_anggota`),
  KEY `id_jabatan` (`id_jabatan`),
  KEY `id_tahun` (`id_tahun`),
  CONSTRAINT `t_anggota_jabatan_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `t_anggota` (`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_anggota_jabatan_ibfk_2` FOREIGN KEY (`id_jabatan`) REFERENCES `t_jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_anggota_jabatan_ibfk_3` FOREIGN KEY (`id_tahun`) REFERENCES `t_tahun` (`id_tahun`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_anggota_jabatan: ~19 rows (approximately)
/*!40000 ALTER TABLE `t_anggota_jabatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_anggota_jabatan` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_belanja
CREATE TABLE IF NOT EXISTS `t_belanja` (
  `id_belanja` int(5) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(5) NOT NULL,
  `id_kategori` int(5) NOT NULL,
  `belanja` text NOT NULL,
  `volume` int(4) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `harga` int(20) NOT NULL,
  `jumlah` int(20) NOT NULL,
  PRIMARY KEY (`id_belanja`),
  KEY `id_kategori` (`id_kategori`),
  KEY `id_kegiatan` (`id_kegiatan`),
  CONSTRAINT `t_belanja_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `t_kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_belanja_ibfk_3` FOREIGN KEY (`id_kegiatan`) REFERENCES `t_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=371 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_belanja: ~355 rows (approximately)
/*!40000 ALTER TABLE `t_belanja` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_belanja` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_bidang
CREATE TABLE IF NOT EXISTS `t_bidang` (
  `id_bidang` int(5) NOT NULL AUTO_INCREMENT,
  `mak_bidang` varchar(10) NOT NULL,
  `nama_bidang` varchar(200) NOT NULL,
  `tap_biaya` decimal(20,0) NOT NULL,
  `id_tahun` int(5) NOT NULL,
  PRIMARY KEY (`id_bidang`),
  KEY `id_tahun` (`id_tahun`),
  CONSTRAINT `t_bidang_ibfk_1` FOREIGN KEY (`id_tahun`) REFERENCES `t_tahun` (`id_tahun`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_bidang: ~19 rows (approximately)
/*!40000 ALTER TABLE `t_bidang` DISABLE KEYS */;
INSERT INTO `t_bidang` (`id_bidang`, `mak_bidang`, `nama_bidang`, `tap_biaya`, `id_tahun`) VALUES
	(4, '11', 'BIDANG PEGAWAI', 100000, 1),
	(5, '12', 'BIDANG HUMAS dan SDI', 830000000, 1),
	(6, '13', 'BIDANG PEDIDIKAN, PENGAJARAN DAN EVALUASI', 1470840280, 1),
	(7, '14', 'BIDANG KEGIATAN SISWA', 1469500000, 1),
	(8, '15', 'PEMBINAAN KEISLAMAN', 274825000, 1),
	(9, '16', 'BIDANG KEPUSTAKAAN', 323860938, 1),
	(10, '17', 'BIDANG LABORATORIUM', 214458400, 1),
	(11, '18', 'KANTOR DAN ALAT TULIS', 259092000, 1),
	(12, '19', 'BIDANG PENGADAAN INVENTARIS', 330000000, 1),
	(13, '20', 'BIAYA LANGGANAN DAN JASA', 513672382, 1),
	(14, '21', 'BIDANG PERAWATAN GEDUNG DAN HALAMAN', 645000000, 1),
	(15, '22', 'BIDANG PERAWATAN INVENTARIS', 100000000, 1),
	(16, '23', 'BIDANG PERAWATAN KENDARAAN', 125000000, 1),
	(17, '24', 'PERJALANAN DINAS', 80200000, 1),
	(18, '25', 'BIDANG AKOMODASI ASRAMA', 4309350000, 1),
	(19, '26', 'BIDANG KESEJAHTERAAN PEGAWAI DAN SISWA', 1792786000, 1),
	(20, '27', 'BANTUAN DAN SUMBANGAN', 70000000, 1),
	(21, '28', 'BIAYA LAIN - LAIN', 137000000, 1),
	(22, '15', 'BIDANG PEMBINAAN BAHASA ARAB DAN INGGRIS DI ASRAMA', 251000000, 1);
/*!40000 ALTER TABLE `t_bidang` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_detail
CREATE TABLE IF NOT EXISTS `t_detail` (
  `id_detail` int(5) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(5) NOT NULL,
  `pelaksana` varchar(200) NOT NULL,
  `sasaran` varchar(200) NOT NULL,
  `lokasi` varchar(200) NOT NULL,
  `capaian` varchar(200) NOT NULL,
  `sumber_dana` varchar(50) NOT NULL,
  `tenaga` varchar(50) NOT NULL,
  `waktu` varchar(50) NOT NULL,
  `keluaran` text NOT NULL,
  `target_capaian` varchar(20) NOT NULL,
  PRIMARY KEY (`id_detail`),
  KEY `t_detail_ibfk_1` (`id_kegiatan`),
  CONSTRAINT `t_detail_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `t_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_detail: ~91 rows (approximately)
/*!40000 ALTER TABLE `t_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_detail` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_jabatan
CREATE TABLE IF NOT EXISTS `t_jabatan` (
  `id_jabatan` int(5) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(200) NOT NULL,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_jabatan: ~19 rows (approximately)
/*!40000 ALTER TABLE `t_jabatan` DISABLE KEYS */;
INSERT INTO `t_jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
	(4, 'Direktur'),
	(5, 'Wakil Direktur 1'),
	(6, 'Wakil Direktur 2'),
	(7, 'Wakil Direktur 3'),
	(8, 'Wakil Direktur 4'),
	(10, 'Bendahara Pengguna'),
	(11, 'Kepala Tata Usaha'),
	(12, 'Staff Urusan Pendidikan dan Pengajaran'),
	(13, 'Staff Urusan Perpustakaan dan Sumber Belajar'),
	(14, 'Staff Urusan Sarana, Keamanan dan Ketertiban'),
	(15, 'Staff Urusan Kerumahtanggaan'),
	(16, 'Satff Urusan Humas dan Sumber Daya Insani'),
	(17, 'Staff Urusan Perkaderan dan Alumni'),
	(18, 'Staff Urusan Bimbingan Siswa'),
	(19, 'Staff Urusan Pembinaan Kegiatan dan Prestasi'),
	(20, 'Staff Urusan Bimbingam Kehidupan Islami'),
	(21, 'Staff Urusan Bimbingan Bahasa'),
	(22, 'Staff Urusan Laboratorium'),
	(23, 'Admin');
/*!40000 ALTER TABLE `t_jabatan` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_kategori
CREATE TABLE IF NOT EXISTS `t_kategori` (
  `id_kategori` int(5) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(200) NOT NULL,
  `kode_kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_kategori: ~5 rows (approximately)
/*!40000 ALTER TABLE `t_kategori` DISABLE KEYS */;
INSERT INTO `t_kategori` (`id_kategori`, `nama_kategori`, `kode_kategori`) VALUES
	(2, 'Inventaris', 'A.1'),
	(3, 'ATK', 'A.2'),
	(4, 'Honor Kepanitian', 'B.1'),
	(5, 'Konsumsi', 'B.2'),
	(6, 'Lain - lainnya', 'L.1');
/*!40000 ALTER TABLE `t_kategori` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_kegiatan
CREATE TABLE IF NOT EXISTS `t_kegiatan` (
  `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_program` int(11) NOT NULL,
  `mak_kegiatan` varchar(10) NOT NULL,
  `nama_kegiatan` varchar(200) NOT NULL,
  `tap_kegiatan` decimal(20,0) NOT NULL,
  `id_anggota` int(5) NOT NULL,
  PRIMARY KEY (`id_kegiatan`),
  KEY `id_program` (`id_program`),
  KEY `id_anggota` (`id_anggota`),
  CONSTRAINT `t_kegiatan_ibfk_1` FOREIGN KEY (`id_program`) REFERENCES `t_program` (`id_program`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_kegiatan_ibfk_2` FOREIGN KEY (`id_anggota`) REFERENCES `t_anggota` (`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_kegiatan: ~228 rows (approximately)
/*!40000 ALTER TABLE `t_kegiatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_kegiatan` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_level
CREATE TABLE IF NOT EXISTS `t_level` (
  `id_level` int(5) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(100) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_level: ~3 rows (approximately)
/*!40000 ALTER TABLE `t_level` DISABLE KEYS */;
INSERT INTO `t_level` (`id_level`, `nama_level`) VALUES
	(1, 'Admin'),
	(2, 'Operator'),
	(3, 'Guest');
/*!40000 ALTER TABLE `t_level` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_panitia
CREATE TABLE IF NOT EXISTS `t_panitia` (
  `id_panitia` int(5) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(5) NOT NULL,
  `id_struktur` int(5) NOT NULL,
  `id_kegiatan` int(5) NOT NULL,
  `id_tahun` int(5) NOT NULL,
  PRIMARY KEY (`id_panitia`),
  KEY `id_anggota` (`id_anggota`),
  KEY `id_struktur` (`id_struktur`),
  KEY `id_kegiatan` (`id_kegiatan`),
  KEY `id_tahun` (`id_tahun`),
  CONSTRAINT `t_panitia_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `t_anggota` (`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_panitia_ibfk_2` FOREIGN KEY (`id_struktur`) REFERENCES `t_struktur` (`id_struktur`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_panitia_ibfk_3` FOREIGN KEY (`id_kegiatan`) REFERENCES `t_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_panitia_ibfk_4` FOREIGN KEY (`id_tahun`) REFERENCES `t_tahun` (`id_tahun`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_panitia: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_panitia` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_panitia` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_pembelanjaan
CREATE TABLE IF NOT EXISTS `t_pembelanjaan` (
  `id_pembelanjaan` int(5) NOT NULL AUTO_INCREMENT,
  `id_pengeluaran` int(5) NOT NULL,
  `tanggal_pembelanjaan` date NOT NULL,
  `uraian_pembelanjaan` varchar(100) NOT NULL,
  `jumlah_pembelanjaan` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id_pembelanjaan`),
  KEY `id_pengeluaran` (`id_pengeluaran`),
  CONSTRAINT `t_pembelanjaan_ibfk_1` FOREIGN KEY (`id_pengeluaran`) REFERENCES `t_pengeluaran` (`id_pengeluaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_pembelanjaan: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pembelanjaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pembelanjaan` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_pengajuan
CREATE TABLE IF NOT EXISTS `t_pengajuan` (
  `id_pengajuan` int(5) NOT NULL AUTO_INCREMENT,
  `tanggal_pengajuan` date NOT NULL,
  `tanggal_lpj` date NOT NULL,
  `id_verifikasi` int(5) NOT NULL,
  `status_pengajuan` int(1) NOT NULL,
  `id_kegiatan` int(5) NOT NULL,
  `tahap_pengajuan` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_pengajuan`),
  KEY `id_anggota` (`id_verifikasi`),
  KEY `id_kegiatan` (`id_kegiatan`),
  CONSTRAINT `t_pengajuan_ibfk_2` FOREIGN KEY (`id_verifikasi`) REFERENCES `t_anggota` (`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_pengajuan_ibfk_3` FOREIGN KEY (`id_kegiatan`) REFERENCES `t_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_pengajuan: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pengajuan` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pengajuan` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_pengeluaran
CREATE TABLE IF NOT EXISTS `t_pengeluaran` (
  `id_pengeluaran` int(5) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(5) NOT NULL,
  `id_kegiatan` int(5) NOT NULL,
  `no_kk` int(20) NOT NULL,
  `tanggal_pengeluaran` date NOT NULL,
  `tahap_pengeluaran` int(1) NOT NULL DEFAULT 0,
  `id_verifikator` int(5) NOT NULL,
  PRIMARY KEY (`id_pengeluaran`),
  KEY `id_pengajuan` (`id_pengajuan`),
  KEY `id_kegiatan` (`id_kegiatan`),
  KEY `id_verifikator` (`id_verifikator`),
  CONSTRAINT `t_pengeluaran_ibfk_1` FOREIGN KEY (`id_pengajuan`) REFERENCES `t_pengajuan` (`id_pengajuan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_pengeluaran_ibfk_2` FOREIGN KEY (`id_kegiatan`) REFERENCES `t_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_pengeluaran_ibfk_3` FOREIGN KEY (`id_verifikator`) REFERENCES `t_anggota` (`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_pengeluaran: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pengeluaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pengeluaran` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_program
CREATE TABLE IF NOT EXISTS `t_program` (
  `id_program` int(5) NOT NULL AUTO_INCREMENT,
  `id_bidang` int(5) NOT NULL,
  `mak_program` varchar(10) NOT NULL,
  `nama_program` varchar(100) NOT NULL,
  `tap_program` decimal(20,0) NOT NULL,
  `id_anggota` int(5) NOT NULL,
  PRIMARY KEY (`id_program`),
  KEY `id_bidang` (`id_bidang`),
  KEY `id_anggota` (`id_anggota`),
  CONSTRAINT `t_program_ibfk_1` FOREIGN KEY (`id_bidang`) REFERENCES `t_bidang` (`id_bidang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_program_ibfk_2` FOREIGN KEY (`id_anggota`) REFERENCES `t_anggota` (`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_program: ~110 rows (approximately)
/*!40000 ALTER TABLE `t_program` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_program` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_rincian
CREATE TABLE IF NOT EXISTS `t_rincian` (
  `id_rincian` int(5) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` int(5) NOT NULL,
  `uraian_rincian` varchar(100) NOT NULL,
  `jumlah_rincian` decimal(10,0) NOT NULL,
  `tanggal_rincian` date NOT NULL,
  PRIMARY KEY (`id_rincian`),
  KEY `id_pengajuan` (`id_pengajuan`),
  CONSTRAINT `t_rincian_ibfk_1` FOREIGN KEY (`id_pengajuan`) REFERENCES `t_pengajuan` (`id_pengajuan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_rincian: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_rincian` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_rincian` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_struktur
CREATE TABLE IF NOT EXISTS `t_struktur` (
  `id_struktur` int(5) NOT NULL AUTO_INCREMENT,
  `nama_struktur` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_struktur`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_struktur: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_struktur` DISABLE KEYS */;
INSERT INTO `t_struktur` (`id_struktur`, `nama_struktur`, `deskripsi`) VALUES
	(1, 'Ketua', 'Memimpin dalam setiap kegiatan atau agenda'),
	(2, 'Wakil', 'Membantu tugas internal ');
/*!40000 ALTER TABLE `t_struktur` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_tahun
CREATE TABLE IF NOT EXISTS `t_tahun` (
  `id_tahun` int(5) NOT NULL AUTO_INCREMENT,
  `nama_tahun` varchar(200) NOT NULL,
  PRIMARY KEY (`id_tahun`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_tahun: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_tahun` DISABLE KEYS */;
INSERT INTO `t_tahun` (`id_tahun`, `nama_tahun`) VALUES
	(1, 'Tahun Ajaran 2020/2021');
/*!40000 ALTER TABLE `t_tahun` ENABLE KEYS */;

-- Dumping structure for table sdpa.t_user
CREATE TABLE IF NOT EXISTS `t_user` (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `id_level` int(5) NOT NULL,
  `id_anggota` int(5) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_level` (`id_level`),
  KEY `t_user_ibfk_2` (`id_anggota`),
  CONSTRAINT `t_user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `t_level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_user_ibfk_2` FOREIGN KEY (`id_anggota`) REFERENCES `t_anggota` (`id_anggota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table sdpa.t_user: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` (`id_user`, `username`, `password`, `id_level`, `id_anggota`) VALUES
	(5, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 21);
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
